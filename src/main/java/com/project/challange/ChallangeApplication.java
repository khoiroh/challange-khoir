package com.project.challange;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@Configuration
@EnableJpaAuditing
@SpringBootApplication
public class ChallangeApplication{

	public static void main(String[] args) {
		SpringApplication.run(ChallangeApplication.class, args);
	}

}
