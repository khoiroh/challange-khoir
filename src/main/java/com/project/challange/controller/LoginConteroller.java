package com.project.challange.controller;

import com.project.challange.model.Login;
import com.project.challange.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

// Untuk menjalankan method
@RestController
@RequestMapping("/login1")
@CrossOrigin(origins = "http://localhost:3000")
public class LoginConteroller {

    @Autowired
    LoginService loginService;

//    method post/sign-in untuk menambahkan login
    @PostMapping("/sign-in")
    public Map<String, Object> login(@RequestBody Login login) {
        return loginService.login(login);
    }

//    method post/sign-up untuk menambahkan register
    @PostMapping("/sign-up")
    public Login addLogin(@RequestBody Login login) {
        return loginService.addLogin(login);
    }

//    method get untuk melihat variable per id
    @GetMapping("/{id}")
    public Login getLoginById(@PathVariable("id") Long id) {
        return loginService.getLoginById(id);
    }

//    method put untuk mengubah variable per id
    @PutMapping("/{id}")
    public Login editLogin(@PathVariable("id") Long id, @RequestBody Login login) {
        return loginService.editLogin(id, login);
    }

//    method get/all untuk melihat semua variable
    @GetMapping("/all")
    public List<Login> getAllLogin() {
        return loginService.getAllLogin();
    }

//    method delete untuk menghapus variable per id
    @DeleteMapping("/{id}")
    public void deleteLoginById(@PathVariable("id") Long id) {
        loginService.deleteLoginById(id);
    }

}
