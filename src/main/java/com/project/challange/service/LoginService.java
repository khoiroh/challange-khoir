package com.project.challange.service;

import com.project.challange.model.Login;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

// untuk membuat method
public interface LoginService {

    Map<String, Object> login(Login login);

    Login addLogin(Login login); // method untuk menambahkan data/variable

    Login getLoginById(Long id); // method untuk melihat data per id

    List<Login> getAllLogin();  // method untuk melihat semua data/variable

//    method untuk mengedit/mengubah data per id
    Login editLogin(Long id, Login login);

    void deleteLoginById(Long id); // method untuk menghapus data per id

}
