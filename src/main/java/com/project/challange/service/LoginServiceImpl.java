package com.project.challange.service;

import com.project.challange.model.Login;
import com.project.challange.repository.LoginRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class LoginServiceImpl implements LoginService{

    @Autowired
    LoginRepository loginRepository;

    @Override
    public Map<String, Object> login(Login login) {
        Login login1 = loginRepository.findByEmail(login.getEmail()).get();
        Map<String, Object> response = new HashMap<>();
        response.put("user", login1);
        return response;
    }

    @Override
    public Login addLogin(Login login) {
        return loginRepository.save(login);
    }

    @Override
    public Login getLoginById(Long id) {
        return loginRepository.findById(id).orElseThrow();
    }

    @Override
    public List<Login> getAllLogin() {
        return loginRepository.findAll();
    }

    @Override
    public Login editLogin(Long id, Login login) {
        Login login1 = loginRepository.findById(id).get();
        login1.setUsername(login.getUsername());
        login1.setEmail(login.getEmail());
        login1.setPassword(login.getPassword());
        return loginRepository.save(login1);
    }

    @Override
    public void deleteLoginById(Long id) {
        loginRepository.deleteById(id);
    }
}
