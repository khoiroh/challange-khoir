package com.project.challange.repository;

import com.project.challange.model.Login;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LoginRepository extends JpaRepository<Login, Long> {

    Optional<Login> findByEmail(String email);

    Boolean existsByEmail(String email);

}
